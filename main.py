import os
import json
import subprocess
import argparse
import logging as log

parser = argparse.ArgumentParser(description="Rolls autobuyer", add_help=True)
parser.add_argument('-p', '--password', required=True, help="Password for massa-client", metavar="")
args = parser.parse_args()

log.basicConfig(
    level=log.INFO,
    datefmt='%d-%m-%Y %H:%M:%S',
    format="%(asctime)s %(levelname)s %(message)s"
    )

def wallet_list(data:dict) -> list:
    wallets_addresses = []
    for address in data.keys():
        wallets_addresses.append(address)
    return wallets_addresses

def enough_for_roll(data:dict) -> dict:
    check_dict = {}
    for address in data.keys():
        balance = float(data[address]["address_info"]["final_balance"])
        if balance >= 100:
            check_dict[address] = {"available": True, "available_to_purchase": int(balance)//100}
        else:
            check_dict[address] = {"available": False, "available_to_purchase": 0}
    return check_dict

def buy_roll(address: str, amount: int) -> str:
    cmd = [ "./massa-client", "-j", "-p", args.password,
        "buy_rolls", address, str(amount), "0" ]
    try:
        result = subprocess.run(cmd, capture_output=True, text=True, check=True)
        json_data = json.loads(result.stdout)
        return f"Operation id {json_data[0]} bought {amount} rolls for {address}"
    except subprocess.CalledProcessError as err:
        return f"Error while asking to buy rolls: {err}" 

cmd = ["./massa-client", "-j", "-p", args.password, "wallet_info"]
try:
    wallet_info = subprocess.run(cmd, capture_output=True, check=True)
except subprocess.CalledProcessError as err:
    log.error(f"Error received: {err}")
    exit(1)

filtered_dict = {key: value for key, value in enough_for_roll(json.loads(wallet_info.stdout)).items() if value.get('available')}
if filtered_dict != {}:
    for address in filtered_dict.keys():
        log.info(buy_roll(address, filtered_dict[address]["available_to_purchase"]))
else:
    log.warning(f"Nothing to buy")        
