# massa_rolls_autobuy

Parsing all your connected to node wallet and buy rolls if it has free 100+ mass coin.
**It is nessecary to place script to massa-client directory**

## Usage
```bash
python3 main.py -p massa_client_password
```

```bash
usage: main.py [-h] -p

Rolls autobuyer

options:
  -h, --help        show this help message and exit
  -p , --password   Password for massa-client
```
## Crontab jon

```
00 * * * * cd <MASSA_CLIENT_DIR_PATH>; usr/bin/python3 main.py <MASSA_CLIENT_PASSWORD>
```